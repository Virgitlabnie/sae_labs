package fr.centralesupelec.galtier.wikimedia.proxy;

import java.io.Reader;
import java.io.StringReader;
import java.net.URI;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.Producer;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.clients.producer.ProducerRecord;

import com.fasterxml.jackson.annotation.JsonAutoDetect.Visibility;
import com.fasterxml.jackson.annotation.PropertyAccessor;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.launchdarkly.eventsource.EventHandler;
import com.launchdarkly.eventsource.EventSource;
import com.launchdarkly.eventsource.MessageEvent;

import fr.centralesupelec.galtier.wikimedia.recentChange.RecentChange;

/**
 * @author V. Galtier 
 * 
 * Act as a proxy to Wikimedia changes web stream for the Kafka-based application:
 * Read the stream of Wikimedia changes and write them to a kafka topic.
 * The stream-reading part is derived from
 * https://golb.hplar.ch/2018/02/Access-Server-Sent-Events-from-Java.html
 */
public class Proxy implements EventHandler {

	 
	// configure logger
	private static Logger logger = null;
	static {
		System.setProperty("java.util.logging.SimpleFormatter.format",
				"[%1$tT] [%4$-7s] %5$s %n");
		logger = LoggerFactory.getLogger(Proxy.class.getName());
	}

	/**
	 * Kafka producer
	 */
	private Producer<Integer, String> producer;
	/**
	 * Kafka name of the destination topic
	 */
	private static String topicName;

	/**
	 * @param args first argument is a list of Kafka bootstrap servers,
	 *             second argument is the name of the Kafka destination topic
	 */
	public static void main(String[] args) {
		String bootstrapServersList = args[0];
		topicName = args[1];
		new Proxy(bootstrapServersList);
	}

	/**
	 * Create a stream listener and a Kafka producer.
	 *
	 * @param bootstrappingServersList list of Kafka bootstrapping servers
	 */
	Proxy(String bootstrappingServersList) {
		String url = "https://stream.wikimedia.org/v2/stream/recentchange";
		EventSource.Builder builder = new EventSource.Builder(this, URI.create(url));
		EventSource eventSource = builder.build();
		eventSource.start();

		Properties producerProperties = configureKafkaProducer(bootstrappingServersList);
		producer = new KafkaProducer<>(producerProperties);

		// listen to the stream for 30 seconds
		/*
		 * try { TimeUnit.SECONDS.sleep(30); } catch (InterruptedException e) {
		 * e.printStackTrace(); }
		 */
		// listen to the stream for ever
		while (true) {
			// empty on purpose
		}
	}

	/**
	 * Prepare configuration for the Kafka producer.
	 * 
	 * @param bootstrappingServersList list of Kafka bootstrapping servers
	 * @return configuration properties for the Kafka producer
	 */
	private Properties configureKafkaProducer(String bootstrappingServersList) {
		Properties producerProperties = new Properties();

		producerProperties.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, bootstrappingServersList);
		producerProperties.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, 
				org.apache.kafka.common.serialization.IntegerSerializer.class.getName());
		producerProperties.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, 
				org.apache.kafka.common.serialization.StringSerializer.class.getName());

		return producerProperties;
	}

	/**
	 * When a message comes on the stream, write it to the Kafka topic.
	 */
	@Override
	public void onMessage(String event, MessageEvent messageEvent) throws Exception {
		String message = messageEvent.getData();
		
		final ObjectMapper objectMapper = new ObjectMapper();
		objectMapper.setVisibility(PropertyAccessor.FIELD, Visibility.ANY);
		objectMapper.configure(DeserializationFeature.ACCEPT_SINGLE_VALUE_AS_ARRAY, true);
		Reader reader = new StringReader(message);
		RecentChange recentChange = objectMapper.readValue(reader, RecentChange.class);
		
		String jsonString = objectMapper.writeValueAsString(recentChange);
		
		// JSON schema:
		// https://github.com/wikimedia/mediawiki-event-schemas/blob/master/jsonschema/mediawiki/recentchange/1.0.0.json
		producer.send(new ProducerRecord<>(topicName, null, jsonString));
		//logger.info("sent to wikimediachanges topic: " + jsonString);	
	}

	@Override
	public void onOpen() throws Exception {
		logger.info("The stream connection has been opened.");
	}

	@Override
	public void onClosed() throws Exception {
		logger.info("The stream connection has been closed.");
	}

	@Override
	public void onComment(String comment) throws Exception {
		logger.info("A comment line (line starting with a colon) was received from the stream: " + comment);
	}

	@Override
	public void onError(Throwable t) {
		logger.info("An exception occured on the socket connection: " + t.getMessage());
	}
}
