package fr.centralesupelec.galtier.wikimedia.aggregator;

import java.time.Duration;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Properties;
import java.util.Vector;
import java.util.logging.Logger;

import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.common.PartitionInfo;
import org.apache.kafka.common.TopicPartition;
import org.json.JSONObject;

/**
 * @author V. Galtier
 *
 * Reads the "wikipediachanges" topic, identifies and displays 
 * the 5-top active regions on tumbling windows.
 */
public class Aggregator {

	// configure logger
	private static Logger logger = null;
	static {
		System.setProperty("java.util.logging.SimpleFormatter.format",
				"[%1$tT] [%4$-7s] %5$s %n");
		logger = Logger.getLogger(Aggregator.class.getName());
	}

	public static void main(String[] args) {
		String bootstrappingServersList = args[0];
		String wikipediaTopic = args[1];
		int windowsSec = Integer.parseInt(args[2]);
		new Aggregator(bootstrappingServersList, wikipediaTopic, windowsSec);
	}


	public Aggregator(String bootstrappingServersList, String wikipediaTopic, int windowsSec) {

		KafkaConsumer<Integer, String> consumer = new KafkaConsumer(configureKafkaConsumer(bootstrappingServersList));
		// version 1
		// read from any partition of the topic
		// (and hopefully all! should be alone in its group)
		//consumer.subscribe(Collections.singletonList(wikipediaTopic));

		// version 2
		// comment out the following line in the configureKafkaConsumer method:
		// consumerProperties.put(ConsumerConfig.GROUP_ID_CONFIG, "the_aggregator");
		// Get metadata about partitions for all topics that the user is authorized to view. 
		Map<String, List<PartitionInfo>> topicsMap = consumer.listTopics();
		// Focus on the wikipediachanges topic
		List<PartitionInfo> wikipediaPartitions = topicsMap.get(wikipediaTopic);
		// add each partition to the list of partitions to assign
		Collection<TopicPartition> partitions = new Vector<TopicPartition>();
		for (int i=0; i<wikipediaPartitions.size(); i++) {
			partitions.add(new TopicPartition(wikipediaTopic, wikipediaPartitions.get(i).partition()));
		}

		consumer.assign(partitions);
		
		/**
		 * list of the regions subject to changes along with the number of changes
		 * key: region, value: number of times this region came up in the topic
		 */
		Map<String, Integer> regions = new HashMap<>();

		long timeStone = System.currentTimeMillis();

		try {
			Duration timeout = Duration.ofMillis(1000);
			while (true) {
				// read from topic
				ConsumerRecords<Integer, String> records = consumer.poll(timeout);
				for (ConsumerRecord<Integer, String> record : records) {
					JSONObject jsonObjectRoot = new JSONObject(record.value());
					String region = jsonObjectRoot.getString("region");
					// new or existing region?
					if (regions.containsKey(region)) {
						regions.put(region, 1+regions.get(region));
					} else {
						regions.put(region, 1);
					}


					if (System.currentTimeMillis() - timeStone > windowsSec*1000) {
						timeStone = System.currentTimeMillis();
						displayTop(regions);
						regions = new HashMap<>();
					}
				}

			}
		} catch (Exception e) {
			logger.severe("something went wrong... " + e.getMessage());
			System.exit(1);
		} finally {
			consumer.close();
		}	
	}


	private void displayTop(Map<String, Integer> regions) {
		// sort the regions list in reverseOrder (region key with the highest value first)
		// version 1: available at source level 1.8 or above:
		Iterator<Entry<String, Integer>> regionsIterator = regions.entrySet()
				.stream()
				.sorted(Map.Entry.comparingByValue(Comparator.reverseOrder()))
				.iterator();
		// end version 1
		/*
		// version 2:
		// 1. convert Map to list of maps
        List<Map.Entry<String, Integer>> list = new LinkedList<Map.Entry<String, Integer>>(regions.entrySet());
        // 2. Sort list with Collections.sort(), provide a custom Comparator
        Collections.sort(list, new Comparator<Map.Entry<String, Integer>>() {
            public int compare(Map.Entry<String, Integer> o1,
                               Map.Entry<String, Integer> o2) {
                return (o2.getValue()).compareTo(o1.getValue());
            }
        });
        // 3. Loop the sorted list and put it into a new insertion order Map LinkedHashMap
        Map<String, Integer> sortedMap = new LinkedHashMap<String, Integer>();
        for (Map.Entry<String, Integer> entry : list) {
            sortedMap.put(entry.getKey(), entry.getValue());
        }
        Iterator<Entry<String, Integer>> regionsIterator = sortedMap.entrySet().iterator();
		// end version 2
		*/
		
		
		
		
		int NB_REGIONS_TO_LOG = 5;

		int j=0;
		Entry<String, Integer> entry = null;

		while((regionsIterator.hasNext())&&(j<NB_REGIONS_TO_LOG)) {
			entry = regionsIterator.next();
			logger.info(entry.getKey() + "\t\t" + entry.getValue());
			j++;
		}
		logger.info("=================================");
	}


	/**
	 * Prepares configuration for the Kafka consumer.
	 * 
	 * @param bootstrappingServersList the list of Kafka bootstrapping servers
	 * @return configuration properties for the Kafka consumer
	 */
	private Properties configureKafkaConsumer(String bootstrappingServersList) {
		Properties consumerProperties = new Properties();

		consumerProperties.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG,bootstrappingServersList);
		consumerProperties.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, 
				org.apache.kafka.common.serialization.VoidDeserializer.class.getName());
		consumerProperties.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, 
				org.apache.kafka.common.serialization.StringDeserializer.class.getName());
		//consumerProperties.put(ConsumerConfig.GROUP_ID_CONFIG, "the_aggregator");
		consumerProperties.put(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, "earliest"); // from beginning

		return consumerProperties;
	}
}



