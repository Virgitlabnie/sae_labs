if $KAFKA_HOME/bin/kafka-topics.sh --list --bootstrap-server localhost:9092 | grep -q wikimediachanges; then
   echo "delete existing wikimediachanges topic"
   $KAFKA_HOME/bin/kafka-topics.sh --bootstrap-server localhost:9092 --delete --topic wikimediachanges
else
   echo "there was no wikimediachanges topic yet"
fi
$KAFKA_HOME/bin/kafka-topics.sh --create --bootstrap-server localhost:9092 --partitions 2 --config retention.ms=3600000 --topic wikimediachanges

if $KAFKA_HOME/bin/kafka-topics.sh --list --bootstrap-server localhost:9092 | grep -q wikipediachanges; then
   echo "delete existing wikipediachanges topic"
   $KAFKA_HOME/bin/kafka-topics.sh --bootstrap-server localhost:9092 --delete --topic wikipediachanges
else
   echo "there was no wikipediachanges topic yet"
fi
$KAFKA_HOME/bin/kafka-topics.sh --create --bootstrap-server localhost:9092 --partitions 1 --config retention.ms=3600000 --topic wikipediachanges
