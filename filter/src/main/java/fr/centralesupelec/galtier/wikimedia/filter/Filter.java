package fr.centralesupelec.galtier.wikimedia.filter;

import java.time.Duration;
import java.util.Collections;
import java.util.Properties;
import java.util.logging.Logger;

import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.json.JSONObject;

/**
 * @author V. Galtier
 *
 * Reads records from the topic wikimediachanges and sends to the topic wikipediachanges 
 * the region ("fr", "en"...) of the records that indicate a change in Wikipedia
 */
public class Filter {

	// configure logger
	private static Logger logger = null;
	static {
		System.setProperty("java.util.logging.SimpleFormatter.format",
				"[%1$tT] [%4$-7s] %5$s %n");
		logger = Logger.getLogger(Filter.class.getName());
	}
	
	public static void main(String[] args) {
		String bootstrappingServersList = args[0];
		// upstream and downstream topics names
		String wikimediaTopic = args[1];
		String wikipediaTopic = args[2];
		new Filter(bootstrappingServersList, wikimediaTopic, wikipediaTopic);
	}


	public Filter(String bootstrappingServersList, String wikimediaTopic, String wikipediaTopic) {

		KafkaConsumer<Object, String> consumer = new KafkaConsumer<Object, String>(configureKafkaConsumer(bootstrappingServersList));
		// read from any partition of the topic
		consumer.subscribe(Collections.singletonList(wikimediaTopic));
		
		KafkaProducer<Object, String> producer = new KafkaProducer<Object, String>(configureKafkaProducer(bootstrappingServersList));

		try {
			Duration timeout = Duration.ofMillis(1000);
			while (true) {
				// read from topic
				ConsumerRecords<Object, String> records = consumer.poll(timeout);
				for (ConsumerRecord<Object, String> record : records) {
					// example:
					//{"$schema":"/mediawiki/recentchange/1.0.0","meta":{"uri":"https://en.wikipedia.org/wiki/Kaikhosru_Shapurji_Sorabji",
					// [...] ,"domain":"en.wikipedia.org","stream":"mediawiki.recentchange"},"id":1306308110,[...]}
					JSONObject jsonObjectRoot = new JSONObject(record.value());
					JSONObject jsonObjectMeta = jsonObjectRoot.getJSONObject("meta");
					String domain = jsonObjectMeta.getString("domain");
					// filter and send to downstream topic
					if (domain.contains("wikipedia.org")) {
						String region = (domain.split("\\."))[0];
						String message = "{ \"region\" : \"" + region + " \"}";
						//System.out.println(message);
						producer.send(new ProducerRecord<>(wikipediaTopic, null, message));	
					}
				}
			}
		} catch (Exception e) {
			logger.severe("something went wrong... " + e.getMessage());
			System.exit(1);
		} finally {
			consumer.close();
			producer.close();
		}	
	}


	/**
	 * Prepares configuration for the Kafka consumer.
	 * 
	 * @param bootstrappingServersList list of Kafka bootstrapping servers
	 * @return configuration properties for the Kafka consumer
	 */
	private Properties configureKafkaConsumer(String bootstrappingServersList) {
		Properties consumerProperties = new Properties();

		consumerProperties.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG,bootstrappingServersList);
		consumerProperties.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, 
				org.apache.kafka.common.serialization.VoidDeserializer.class.getName());
		consumerProperties.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, 
				org.apache.kafka.common.serialization.StringDeserializer.class.getName());
		consumerProperties.put(ConsumerConfig.GROUP_ID_CONFIG, "the_filters");
		consumerProperties.put(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, "earliest"); // from beginning

		return consumerProperties;
	}
	
	/**
	 * Prepare configuration for the Kafka producer.
	 * 
	 * @param bootstrappingServersList list of Kafka bootstrapping servers
	 * @return configuration properties for the Kafka producer
	 */
	private Properties configureKafkaProducer(String bootstrappingServersList) {
		Properties producerProperties = new Properties();

		producerProperties.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, bootstrappingServersList);
		producerProperties.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, 
				org.apache.kafka.common.serialization.VoidSerializer.class.getName());
		producerProperties.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, 
				org.apache.kafka.common.serialization.StringSerializer.class.getName());

		return producerProperties;
	}
}
