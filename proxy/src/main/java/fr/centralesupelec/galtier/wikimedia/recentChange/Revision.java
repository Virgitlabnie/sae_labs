package fr.centralesupelec.galtier.wikimedia.recentChange;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyDescription;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.apache.commons.lang.builder.ToStringBuilder;

/**
 * Old and new revision IDs
 *
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "new", "old" })
public class Revision {

	/**
	 * (rc_last_oldid)
	 *
	 */
	@JsonProperty("new")
	@JsonPropertyDescription("(rc_last_oldid)")
	private Integer _new;
	/**
	 * (rc_this_oldid)
	 *
	 */
	@JsonProperty("old")
	@JsonPropertyDescription("(rc_this_oldid)")
	private Integer old;

	/**
	 * No args constructor for use in serialization
	 *
	 */
	public Revision() {
	}

	/**
	 *
	 * @param old
	 * @param _new
	 */
	public Revision(Integer _new, Integer old) {
		super();
		this._new = _new;
		this.old = old;
	}

	/**
	 * (rc_last_oldid)
	 *
	 */
	@JsonProperty("new")
	public Integer getNew() {
		return _new;
	}

	/**
	 * (rc_last_oldid)
	 *
	 */
	@JsonProperty("new")
	public void setNew(Integer _new) {
		this._new = _new;
	}

	/**
	 * (rc_this_oldid)
	 *
	 */
	@JsonProperty("old")
	public Integer getOld() {
		return old;
	}

	/**
	 * (rc_this_oldid)
	 *
	 */
	@JsonProperty("old")
	public void setOld(Integer old) {
		this.old = old;
	}

	@Override
	public String toString() {
		return new ToStringBuilder(this).append("_new", _new).append("old", old).toString();
	}

}
